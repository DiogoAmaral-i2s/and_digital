using AndDigitalChallenge.Domain;
using AndDigitalChallenge.Model;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class PhoneNumberTest
    {
        private readonly ICustomerDomain ICustomerDomain = new CustomerDomain();
        private readonly IPhoneNumberDomain IPhoneNumberDomain = new PhoneNumberDomain();
        [Test]
        public void UpdateStatusPhoneNumber()
        {
            Customers c = new Customers();
            c.Id = 11;
            c.Name = "Daniel";
            c.IsCurrentlyCustomer = true;

           Assert.AreEqual("Daniel", ICustomerDomain.CreateNewCustomer(c).Name);
           Assert.AreEqual(true, ICustomerDomain.CreateNewCustomer(c).IsCurrentlyCustomer);
        }

        [Test]
        public void activePhoneNumber()
        {
            Phones p = new Phones();
            p.PhoneNumber = "00441612421564";
            p.PhoneActive =true;
            p.CustomerId = 2;

            Assert.AreEqual(2, IPhoneNumberDomain.activePhoneNumber(p).CustomerId);
            Assert.AreEqual(true, IPhoneNumberDomain.activePhoneNumber(p).PhoneActive);
            Assert.AreEqual("00441612421564", IPhoneNumberDomain.activePhoneNumber(p).PhoneNumber);
        }
    }
}