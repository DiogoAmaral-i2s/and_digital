﻿using AndDigitalChallenge.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AndDigitalChallenge.Infrastructure
{
    public class DataGenerator
    {
        public static void LoadData(TelecomProviderContext context)
        {
                context.PhoneNumbers.AddRange(
                    new Phones
                    {

                        PhoneActive = false,
                        PhoneNumber = "+441612421564",
                        Customer = null,
                        CustomerId = null,
                    },
                    new Phones
                    {
                        CustomerId = 2,
                        PhoneActive = true,
                        PhoneNumber = "+441612421563",
                        Customer = new Customers
                        {
                            Id = 2,
                            Name = "Nasar Mahmood",
                            IsCurrentlyCustomer = true
                        }
                    },
                    new Phones
                    {
                        CustomerId = 1,
                        PhoneActive = true,
                        PhoneNumber = "+351910719796",
                        Customer = new Customers
                        {
                            Id = 1,
                            Name = "Diogo Amaral",
                            IsCurrentlyCustomer = true
                        }
                    }
                    ,
                    new Phones
                    {
                        CustomerId = 1,
                        PhoneActive = true,
                        PhoneNumber = "+351910719797"
                    }
                    );

                context.SaveChanges();


        }
    }
}
