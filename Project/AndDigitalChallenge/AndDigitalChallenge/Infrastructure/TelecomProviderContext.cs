﻿using AndDigitalChallenge.Model;
using Microsoft.EntityFrameworkCore;

namespace AndDigitalChallenge.Infrastructure
{
    public class TelecomProviderContext : DbContext
    {
        public TelecomProviderContext(DbContextOptions<TelecomProviderContext> options) : base(options)
        {

        }
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<Phones> PhoneNumbers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customers>(entity =>
            {
                //TODO: It is good pratice to use an applyConfiguration
                entity.ToTable("Customers");
                entity.Property(c => c.Id).ValueGeneratedOnAdd();
                entity.HasKey(c => c.Id);
                entity.Property(c => c.Id).HasColumnName("Id");
                entity.Property(c => c.Name).HasColumnName("Name");
                entity.Property(c => c.IsCurrentlyCustomer).HasColumnName("IsCurrentlyCustomer");

                //Relation
                entity.HasMany(d => d.PhoneNumbers)
                  .WithOne(p => p.Customer)
                  .HasForeignKey(d => d.CustomerId);

            });

            modelBuilder.Entity<Phones>(entity =>
            {
                entity.ToTable("Phones");
                entity.HasKey(p => p.PhoneNumber);
                entity.Property(p => p.PhoneNumber).HasColumnName("PhoneNumber");
                entity.Property(p => p.PhoneActive).HasColumnName("PhoneActive");
                entity.Property(p => p.CustomerId).HasColumnName("CustomerId");
                
                //Relation
                entity.HasOne(d => d.Customer)
                  .WithMany(p => p.PhoneNumbers)
                  .HasForeignKey(d => d.CustomerId);
            });
        }
    }
}
