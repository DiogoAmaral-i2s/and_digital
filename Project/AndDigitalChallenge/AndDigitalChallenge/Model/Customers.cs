﻿using System.Collections.Generic;

namespace AndDigitalChallenge.Model
{
    public class Customers
    {
        public Customers()
        {
            PhoneNumbers = new HashSet<Phones>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsCurrentlyCustomer { get; set; }

        public ICollection<Phones> PhoneNumbers { get; set; }
    }
}
