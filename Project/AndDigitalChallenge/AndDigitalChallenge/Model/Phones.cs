﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AndDigitalChallenge.Model
{
    public class Phones
    {
        [RegularExpression(@"((?:\+|00)[17](?: |\-)?|(?:\+|00)[1-9]\d{0,2}(?: |\-)?|(?:\+|00)1\-\d{3}(?: |\-)?)?(0\d|\([0-9]{3}\)|[1-9]{0,3})(?:((?: |\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\-)[0-9]{3}(?: |\-)[0-9]{4})|([0-9]{7}))", ErrorMessage = "It isn't a valid number.")]
        public string PhoneNumber { get; set; }
        public bool PhoneActive { get; set; }
        public int? CustomerId { get; set; }

        public Customers Customer { get; set; }
    }
}
