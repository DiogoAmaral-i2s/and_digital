﻿using AndDigitalChallenge.Model;
using System;

namespace AndDigitalChallenge.Domain
{
    public class CustomerDomain : ICustomerDomain
    {
        public Customers CreateNewCustomer(Customers c)
        {
            Customers NewCustomer = new Customers();
            NewCustomer.Id = c.Id;
            NewCustomer.Name = c.Name;
            NewCustomer.IsCurrentlyCustomer = true;
            return NewCustomer;
        }
        public int GenerateNewId(int id)
        {
            return id += 1;
        }
    }
}
