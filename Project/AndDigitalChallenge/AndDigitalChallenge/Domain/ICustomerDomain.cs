﻿using AndDigitalChallenge.Model;

namespace AndDigitalChallenge.Domain
{
    public interface ICustomerDomain
    {
        Customers CreateNewCustomer(Customers c);
        int GenerateNewId(int id);
    }
}