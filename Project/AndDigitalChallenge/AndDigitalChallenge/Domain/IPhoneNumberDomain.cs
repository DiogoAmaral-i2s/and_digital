﻿using AndDigitalChallenge.Model;
using System.Collections.Generic;

namespace AndDigitalChallenge.Domain
{
    public interface IPhoneNumberDomain
    {
       List<Phones> createPhoneNumberModelAsync(List<Phones> phoneNumber);
       Phones activePhoneNumber(Phones phoneNumber);
    }
}