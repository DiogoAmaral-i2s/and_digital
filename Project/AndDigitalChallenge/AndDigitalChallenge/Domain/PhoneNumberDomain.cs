﻿using AndDigitalChallenge.Model;
using System.Collections.Generic;

namespace AndDigitalChallenge.Domain
{
    public class PhoneNumberDomain : IPhoneNumberDomain
    {
        public List<Phones> createPhoneNumberModelAsync(List<Phones> phoneNumber)
        {
            List<Phones> phones = new List<Phones>();
            foreach (var item in phoneNumber)
            {
                Phones _p = new Phones();
                _p.PhoneNumber = item.PhoneNumber;
                _p.CustomerId = item.CustomerId;
                _p.PhoneActive = item.PhoneActive;
                if (item.Customer != null)
                {
                    Customers c = new Customers();
                    c.Id = item.Customer.Id;
                    c.IsCurrentlyCustomer = item.Customer.IsCurrentlyCustomer;
                    c.Name = item.Customer.Name;
                    _p.Customer = c;
                }
     
                phones.Add(_p);
            }
            return phones;
        }

        public Phones activePhoneNumber(Phones phoneNumber)
        {
            Phones _NewPhone = new Phones();
            _NewPhone.CustomerId = phoneNumber.CustomerId;
            _NewPhone.PhoneActive = true;
            _NewPhone.PhoneNumber = phoneNumber.PhoneNumber;

            return _NewPhone;
        }
    }
}
