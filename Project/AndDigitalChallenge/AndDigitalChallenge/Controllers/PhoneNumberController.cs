﻿using AndDigitalChallenge.Domain;
using AndDigitalChallenge.Infrastructure;
using AndDigitalChallenge.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AndDigitalChallenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhoneNumberController : ControllerBase
    {
        private readonly TelecomProviderContext _context;
        private readonly IPhoneNumberDomain IPhoneNumberDomain = new PhoneNumberDomain();
        private readonly ICustomerDomain ICustomerDomain = new CustomerDomain();
        public PhoneNumberController(TelecomProviderContext context)
        {
            _context = context;

            if (_context.Customers.Count() == 0)
            {
                DataGenerator.LoadData(_context);
                _context.SaveChanges();
            }
        }

        [Route("")]
        [HttpGet]
        [ProducesResponseType(typeof(List<Phones>), (int)HttpStatusCode.OK)]
        public ActionResult<List<Phones>> PhonesAsync()
        {
            return IPhoneNumberDomain.createPhoneNumberModelAsync(_context.PhoneNumbers.ToList());
        }



        [HttpGet]
        [Route("{customerId}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Phones), (int)HttpStatusCode.OK)]
        public ActionResult<List<Phones>> ItemBycustomerId(int customerId)
        {
            if (customerId <= 0)
            {
                return BadRequest();
            }

            List <Phones> _singleCustomer = IPhoneNumberDomain.createPhoneNumberModelAsync(_context.PhoneNumbers.Where(c => c.CustomerId == customerId).ToList());

            if (_singleCustomer != null)
            {
                return _singleCustomer;
            }

            return NotFound();
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public IActionResult UpdateStatusPhoneNumber([FromBody]Phones phoneToActive)
        {
            Phones PhoneNumber = _context.PhoneNumbers.SingleOrDefault(i => i.PhoneNumber == phoneToActive.PhoneNumber);

            if (PhoneNumber == null)
            {
                return NotFound(new { Message = $"Phone Number with Number {phoneToActive.PhoneNumber} not found." });
            }
            if (PhoneNumber.PhoneActive == true)
            {
                return NotFound(new { Message = $"Phone Number with Number {phoneToActive.PhoneNumber} has already been activated before. Please contact support." });
            }

            Customers Customer = _context.Customers.SingleOrDefault(i => i.Id == phoneToActive.CustomerId);
            //New Customer
            if (Customer == null)
            {
                phoneToActive.CustomerId = phoneToActive.Customer.Id = ICustomerDomain.GenerateNewId(_context.Customers.Max(x => x.Id));
               _context.Customers.Add(ICustomerDomain.CreateNewCustomer(phoneToActive.Customer));
               _context.Entry(PhoneNumber).State = EntityState.Detached;
               _context.PhoneNumbers.Update(IPhoneNumberDomain.activePhoneNumber(phoneToActive));
               _context.SaveChanges();
            }
            //Add new contact an existing Customer
            else
            {
                _context.Entry(PhoneNumber).State = EntityState.Detached;
                _context.PhoneNumbers.Update(IPhoneNumberDomain.activePhoneNumber(phoneToActive));
                _context.SaveChanges();
            }
            return new JsonResult(phoneToActive);
        }


    }
}
